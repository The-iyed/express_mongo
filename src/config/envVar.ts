export const corsUrl = process.env.CORS_URL;
export const environment = process.env.NODE_ENV || '';
export const logDirectory = process.env.LOG_DIR || '';
export const port = process.env.PORT || '';
export const baseUrl = process.env.BASE_URL || '';
export const db = {
    name: process.env.DB_NAME || '',
    password : process.env.DB_PASSWORD || '',
    port: process.env.DB_PORT || '',  
    host : process.env.DB_HOST || '',
};
export const cloud_name = process.env.CLOUDINARY_CLOUD_NAME || '';
export const api_key = process.env.CLOUDINARY_API_KEY || '';
export const api_secret = process.env.CLOUDINARY_API_SECRET || '';
export const cloudinaryUrl = process.env.CLOUDINARY_API_ENVIRONMENT_VARIABLE || '';