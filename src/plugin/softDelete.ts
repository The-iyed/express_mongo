import { NextFunction } from 'express';
import mongoose, { ObjectId, Model, Document, Schema } from 'mongoose';
import { ForbiddenError, NotFoundError } from '../helpers/appError';

interface ISoftDeleteModel<T> extends Model<T> {
  anyFindById(
    id: ObjectId | mongoose.Types.ObjectId,
    populate?: string,
    selectFiledPopulate?: string
  ): Promise<T>;
  findDeletebyId(id: ObjectId | mongoose.Types.ObjectId): Promise<T>;
  softDelete(id: ObjectId | mongoose.Types.ObjectId): Promise<T | null>;
  restore(id: ObjectId | mongoose.Types.ObjectId): Promise<T | null>;
}
const softDeletePlugin = (schema: mongoose.Schema, schemaName = 'document'): Schema => {
  schema.add({
    deletedAt: {
      type: Date,
      default: null,
    },
  });

  schema.statics.anyFindById = function (
    id: ObjectId | mongoose.Types.ObjectId,
    populate?: string,
    selectFieldPopulate = '-null'
  ): Promise<Document | null> {
    if (populate) {
      return this.findOne({ _id: id }).populate({ path: populate, select: selectFieldPopulate });
    }

    return this.findOne({ _id: id });
  };
  schema.statics.findDeletebyId = function (
    id: ObjectId | mongoose.Types.ObjectId
  ): Promise<Document | null> {
    return this.findById(id).where('deletedAt').ne(null);
  };

  schema.statics.restore = function (id: ObjectId | mongoose.Types.ObjectId): Promise<Document | null> {
    return this.findByIdAndUpdate(id, { deletedAt: null });
  };
  schema.statics.softDelete = async function (
    id: ObjectId | mongoose.Types.ObjectId
  ): Promise<Document | null> {
    const itemDelete = await this.findById(id);
    if (!itemDelete)
      throw new NotFoundError(`this ${schemaName} has been already deleted permanently or it not found`);
    itemDelete.deletedAt = new Date().toString();
    itemDelete.save();
    return itemDelete;
  };

  schema.post(/^(?!find$)find/, function (result, next: NextFunction) {
    if (result && result.deletedAt) {
      return next(
        new ForbiddenError(
          `this ${schemaName} has been deleted pls contact the support if you want to retrieve`
        )
      );
    }
    next();
  });

  return schema;
};

export { softDeletePlugin, ISoftDeleteModel };
