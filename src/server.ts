import app from "./app";
import { port } from "./config/envVar";
import Logger from "./core/Logger";
import { EMOJIS } from "./constants/emojis";



app
    .listen(port,()=>{
        Logger.info(`server running on port : ${port} ${EMOJIS.CHECK_MARK}`);
    })
    .on('error',(e:Error)=>{
        Logger.error(e);
    })