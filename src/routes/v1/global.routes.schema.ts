import { z } from 'zod';
import { ZodObjectId } from '../../helpers/validator';

export const tokenSchema = z.object({
    token : z.string().nonempty().min(36).max(36)
})


  
export const ObjectIdSchema = z.object({
    id: ZodObjectId
})