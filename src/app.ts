import express, { Request, Response, NextFunction } from 'express';
import cors from 'cors';
import { corsUrl, environment,  } from './config/envVar';
import compression from 'compression';
import hpp from 'hpp';
import { ApiError, InternalError, NotFoundError } from './core/ApiError';
import Logger from './core/Logger';
import router from './routes/v1';
import "./database";

process.on('uncaughtException', (e) => {
    Logger.error(e);
});

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors({ origin: corsUrl, optionsSuccessStatus: 200, credentials: true }));
app.use(hpp());
app.use(compression());
app.use('/api/v1',router)
app.use((err:Error,req:Request,res:Response,next:NextFunction)=> next(new NotFoundError()));
app.use((err:Error , req:Request , res : Response , next:NextFunction)=>{
    if (err instanceof ApiError){
        ApiError.handle(err,res)
    } else {
        if (environment === "development"){
            Logger.error(err)
            return res.status(500).send({status : 'fail' , message : err.message})
        }
        ApiError.handle(new InternalError(),res)
    }
})

export default app;