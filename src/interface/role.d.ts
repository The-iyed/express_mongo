export interface typeRole {
  roleName: string;
  model: string;
  crud: CrudOperationArray;
}
export type CrudOperation = 'create' | 'update' | 'delete' | 'read';
export type CrudOperationArray = Array<CrudOperation>;
