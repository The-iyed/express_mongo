import { Document, ObjectId } from 'mongoose';
import { Parent } from '../database/models/users/Parent';
export interface dataUpdated {
  password: string;
  confirmPassword: string | null;
  isVerified?: boolean;
  photo?: string;
}

export interface userSingUp {
  email: string;
  password: string;
  firstName: string;
  confirmPassword?: string;
  role?: string;
  phoneNumber: string;
  lastName: string;
  country: string;
  codeVerified?: number;
  isVerified?: boolean;
}

export type ParentMongoDb =
  | (Document<unknown, object, Parent> & Omit<Parent & Required<{ _id: ObjectId }>, never>)
  | null;

export type PartialField<T, K extends keyof T> = {
  [P in keyof T]: P extends K ? T[P] | undefined : T[P];
};
export type SelectFields<T> = {
  [K in keyof T]?: 1;
};
