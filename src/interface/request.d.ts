import { Request } from 'express';

import { Admin } from '../database/models/auth/admin';
import { Parent } from '../database/models/users/Parent';

export interface requestWithUser extends Request {
  user: Parent;
}
export interface requestWithAdmin extends Request {
  user: Admin;
}
export interface requestWithSwagger extends Request {
  swaggerDoc: object;
}
