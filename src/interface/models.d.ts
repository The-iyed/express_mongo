import { Document, PaginateModel } from 'mongoose';

import { ISoftDeleteModel } from '../plugin/softDelete';
type SoftDeletePaginateModel<T extends Document> = PaginateModel<T> & ISoftDeleteModel<T>;
