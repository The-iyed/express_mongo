import { ObjectId } from 'mongoose';

export interface adminCredentials {
  fullName: string;
  email: string;
  role: ObjectId;
  password: string;
  confirmPassword: string;
}
