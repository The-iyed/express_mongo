import { z } from 'zod';
import { Request, Response, NextFunction } from 'express';
import Logger from '../core/Logger';
import { BadRequestError } from '../core/ApiError';
import { Types } from 'mongoose';

export enum ValidationSource {
  BODY = 'body',
  HEADER = 'headers',
  QUERY = 'query',
  PARAM = 'params',
  FILE = 'file',
}

type ValidatedRequest<T extends ValidationSource> = Request & { [K in T]: any };


export const ZodObjectId = z.custom((value: string) => {
  if (!Types.ObjectId.isValid(value)) throw new Error('Invalid ObjectId');
  return value;
});

export const ZodUrlEndpoint = z.string().refine(value => !value.includes('://'), {
  message: 'Invalid URL Endpoint',
});

export const ZodAuthBearer = z.string().refine(value => value.startsWith('Bearer ') && value.split(' ')[1], {
  message: 'Invalid Authorization Header',
});

export default <T extends ValidationSource>(
    schema: z.ZodType<any>,
    source: T
  ) => (req: ValidatedRequest<T>, res: Response, next: NextFunction) => {
    try {
      const validationResult = schema.safeParse(req[source]);
  
      if (validationResult.success) {
        return next();
      }
  
      const { error } = validationResult;
      const message = error.issues.map((i) => i.message).join(',');
      Logger.error(message);
  
      next(new BadRequestError(message));
    } catch (error) {
      next(error);
    }
  }