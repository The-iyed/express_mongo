import { HttpCode } from '../constants/httpCode';

export class AppError extends Error {
  public readonly statusCode: number;
  isOperational: boolean;
  constructor(message: string, statusCode: number) {
    super(<string>message);
    this.statusCode = statusCode;
    this.isOperational = true;
    Error.captureStackTrace(this);
  }
}
export class NotFoundError extends Error {
  public readonly statusCode: number;
  isOperational: boolean;
  constructor(message = 'not found') {
    super(<string>message);
    this.statusCode = HttpCode.NOT_FOUND;
    this.isOperational = true;
    Error.captureStackTrace(this);
  }
}

export class UnauthorizedError extends Error {
  public readonly statusCode: number;
  isOperational: boolean;
  constructor(message = 'Unauthorized') {
    super(<string>message);
    this.statusCode = HttpCode.UNAUTHORIZED;
    this.isOperational = true;
    Error.captureStackTrace(this);
  }
}
export class ForbiddenError extends Error {
  public readonly statusCode: number;
  isOperational: boolean;
  constructor(message = 'forbidden') {
    super(<string>message);
    this.statusCode = HttpCode.FORBIDDEN;
    this.isOperational = true;
    Error.captureStackTrace(this);
  }
}
export class BadRequestError extends Error {
  public readonly statusCode: number;
  isOperational: boolean;
  constructor(message = 'Bad Request') {
    super(<string>message);
    this.statusCode = HttpCode.BAD_REQUEST;
    this.isOperational = true;
    Error.captureStackTrace(this);
  }
}

export const handelUnknownError = (error: unknown): boolean => {
  if (
    error instanceof BadRequestError ||
    error instanceof ForbiddenError ||
    error instanceof UnauthorizedError ||
    error instanceof NotFoundError ||
    error instanceof AppError
  )
    return true;
  return false;
};
