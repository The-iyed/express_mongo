import { environment } from "../../config/envVar";
import { seedDelete } from "./drop";
import '../../database'

  
  
export let seed = async(args = {clearDatabase : false}) => {
    if (args.clearDatabase) await seedDelete();
    environment !== 'test' && process.exit(1);

}