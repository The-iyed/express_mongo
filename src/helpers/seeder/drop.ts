import mongoose from 'mongoose';
import { EMOJIS } from '../../constants/emojis';


export let seedDelete = async () => {
    const collections = mongoose.modelNames()
    const deleteCollections = collections.map((collections) => 
    mongoose.models[collections].deleteMany({})
    )
    await Promise.all(deleteCollections)
    console.log('Collections empty successfuly ' + EMOJIS.SUCCESS);

}