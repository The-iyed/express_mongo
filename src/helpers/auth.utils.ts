import jwt, { JwtPayload } from 'jsonwebtoken';
import { ObjectId } from 'mongoose';
import { NextFunction, Request } from 'express';
import { AppError, BadRequestError } from './appError';
import { HttpCode } from '../constants/httpCode';
import crypto from 'crypto';
import bcrypt from 'bcrypt';
import moment from 'moment'

export const generateRefreshToken = (id: ObjectId) => {
  return jwt.sign({ id }, process.env.REFRECH_JWT_SECRET as string, {
    expiresIn: process.env.JWT_REFRESH_EXPIRES_TIME,
  });
};

export const generateAcessToken = (id: ObjectId | JwtPayload) => {
  return jwt.sign(
    {
      id,
    },
    process.env.ACCESS_JWT_SECRET as string,
    {
      expiresIn: process.env.JWT_ACCES_EXPIRES_TIME,
    }
  );
};
export const passwordHashing = async (password: string): Promise<string> => {
  const result = await bcrypt.hash(password, +process.env.PASSWORD_SALT!);
  return result;
};

export const comparePassword = async (currPass: string, userPass: string): Promise<boolean> => {
  const test = await bcrypt.compare(currPass, userPass);
  return test;
};

export const generateCodeVerification = (): { code: number; cryptedCode: string } => {
  const min = 10000;
  const max = 99999;
  const code =
    process.env.NODE_ENV != 'test'
      ? Math.floor(Math.random() * (max - min + 1)) + min
      : Number(process.env.VERIFICATION_CODE!);

  const cryptedCode = crypto.createHash('sha256').update(code.toString()).digest('hex');

  return { code, cryptedCode };
};

export const verifyEmailToken = async (token: string): Promise<JwtPayload> => {
  if (token && process.env.VERIFICATION_EMAIL_TOKEN) {
    return jwt.verify(token, process.env.VERIFICATION_EMAIL_TOKEN) as JwtPayload;
  } else {
    throw new BadRequestError('pls provide you verification token');
  }
};
export const checkExistToken = (req: Request, _: unknown, next: NextFunction): string | void => {
  let token: string | undefined;

  if (req.headers?.authorization && req.headers.authorization.startsWith('Bearer')) {
    token = req.headers.authorization.split(' ')[1];
  }

  if (req?.body?.refreshToken) {
    token = req.body?.refreshToken;
  }

  if (req.cookies.refreshToken) {
    token = req.cookies.refreshToken;
  }
  if (!token) {
    return next(new AppError('You are not logged in! Please log in to get access.', HttpCode.UNAUTHORIZED));
  }

  return token;
};

export const convertDateFormat = (inputDate: string | undefined): string | undefined => {
  const newDate = moment(inputDate, 'MM/DD/YYYY', true);
  if (newDate.isValid()) return newDate.format('YYYY/MM/DD');
  return undefined;
};

