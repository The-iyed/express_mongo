import cloudinary, { UploadApiErrorResponse, UploadApiResponse } from 'cloudinary';
import { v4 as uuidv4 } from 'uuid';
import { ObjectId } from 'mongoose';
import { Request } from 'express';
import { publicRequest } from '../interface/request';
import { AppError } from './appError';
cloudinary.v2.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

export const cloudinaryUserConfig = (
  userId: ObjectId | undefined,
  userType: string,
  query: Request['query'],
  req: publicRequest
) => {
  let user: string = userId?.toString() || uuidv4();
  if (!(userType === 'parent' || userType === 'admin')) user = `${userId || ''}-${Date.now()}`;
  //! temporarily fix
  if (userType === 'countries') user = uuidv4();
  if (userType === 'library' && req.method === 'POST') user = uuidv4();

  if (userType === 'library' && req.method === 'PATCH')
    user = req.body.fileUrl.split('/').pop().split('.')[0];

  const userConfig: cloudinary.UploadApiOptions = {
    public_id: user,
    secure: true,
    width: query.width?.toString() || process.env.WIDTH_USER_PROFILE,
    height: query.height?.toString() || process.env.HEIGHT_USER_PROFILE,
    overwrite: true,
    format: userType != 'shapes' ? query.format?.toString() || process.env.FORMAT_USER_PROFILE : undefined,
    quality: query.quality?.toString() || process.env.QUALITY_USER_PROFILE,
    tags: userType,
    folder: userType,
  };
  if (userType === 'character' || userType === 'levels') {
    return {
      secure: true,
      folder: userType,
    };
  }

  return userConfig;
};
export const cloudinaryCardConfig = (
  userId: ObjectId,
  userType: string,
  req: publicRequest
) => {
  let user: string = userId?.toString() || uuidv4();

  if (!(userType === ('parent' || 'admin'))) {
    user = `${userId || ''}-${Date.now()}`;
  }
  //! temporarily fix
  if (userType === 'countries') user = uuidv4();

  if (userType === 'library' && req.method === 'POST') user = req.body.audioName.trim().split(' ').join('');
  if (userType === 'library' && req.method === 'PATCH') {
    user = req.body.fileUrl.split('/').pop().split('.')[0];
  }

  // make the admin
  const cardConfig: cloudinary.UploadApiOptions = {
    public_id: req.body.title || user,
    folder: userType,
    secure: true,
    tags: userType,
    resource_type: 'video',
    overwrite: true,
    transformation: [
      {
        start_offset: '2.0',
      },
      {
        audio_codec: 'mp3',
        bit_rate: '44k',
      },
    ],
  };

  return cardConfig;
};
export const uploadImgToCloudinary = async (
  image: Buffer,
  req: publicRequest,
  userType: string
) => {
  const uploadPromise = new Promise<void>((resolve, reject) => {
    const stream = cloudinary.v2.uploader.upload_stream(
      cloudinaryUserConfig(req.user?._id, userType, req.query, req),
      (error: UploadApiErrorResponse | undefined, result: UploadApiResponse | undefined) => {
        if (error) {
          reject(error);
          throw new AppError(error.message, error.http_code);
        } else {
          if (req.file && result?.secure_url) {
            req.file.filename = result?.secure_url;
          }
          resolve();
        }
      }
    );
    stream.write(image);
    stream.end();
  });

  return await uploadPromise;
};
export const uploadImgCardToCloudinary = async (
  image: Buffer,
  req: publicRequest,
  userType: string
) => {
  const uploadPromise = new Promise<void>((resolve, reject) => {
    const stream = cloudinary.v2.uploader.upload_stream(
      cloudinaryUserConfig(req.user?._id as ObjectId, userType, req.query, req),
      (error: UploadApiErrorResponse | undefined, result: UploadApiResponse | undefined) => {
        if (error) {
          reject(error);
          throw new AppError(error.message, error.http_code);
        } else {
          if (result) {
            req.body.photo = result.secure_url;
          }
          resolve();
        }
      }
    );
    stream.write(image);
    stream.end();
  });

  return await uploadPromise;
};
export const uploadImgLibraryToCloudinary = (
  image: Buffer,
  req: publicRequest,
  userType: string
) => {
  const uploadPromise = new Promise<void>((resolve, reject) => {
    const stream = cloudinary.v2.uploader.upload_stream(
      cloudinaryUserConfig(req.user?._id, userType, req.query, req),
      (error: UploadApiErrorResponse | undefined, result: UploadApiResponse | undefined) => {
        if (error) {
          reject(error);
          throw new AppError(error.message, error.http_code);
        } else {
          if (result) {
            req.body.photo.push({ url: result.secure_url, name: req.body.photoName });
          }

          resolve();
        }
      }
    );
    stream.write(image);
    stream.end();
  });

  return uploadPromise;
};
export const uploadAudioToCloudinary = async (
  image: Buffer,
  req: publicRequest,
  userType: string
) => {
  const uploadPromise = new Promise<void>((resolve, reject) => {
    const stream = cloudinary.v2.uploader.upload_stream(
      cloudinaryCardConfig(req.user._id, userType, req),
      (error: UploadApiErrorResponse | undefined, result: UploadApiResponse | undefined) => {
        if (error) {
          reject(error);

          throw new AppError(error.message, error.http_code);
        } else {
          if (result) req.body.audio = result.secure_url;

          resolve();
        }
      }
    );

    stream.write(image);
    stream.end();
  });

  return await uploadPromise;
};
export const uploadAudioLibraryCloudinary = async (
  image: Buffer,
  req: publicRequest,
  userType: string
) => {
  const uploadPromise = new Promise<void>((resolve, reject) => {
    const stream = cloudinary.v2.uploader.upload_stream(
      cloudinaryCardConfig(req.user._id, userType, req),
      (error: UploadApiErrorResponse | undefined, result: UploadApiResponse | undefined) => {
        if (error) {
          reject(error);

          throw new AppError(error.message, error.http_code);
        } else {
          if (result) {
            req.body.audio.push({ url: result.secure_url, name: req.body.audioName });
          }

          resolve();
        }
      }
    );

    stream.write(image);
    stream.end();
  });

  return uploadPromise;
};
export const uploadAudioExerciseToCloudinary = async (
  image: Buffer,
  req: publicRequest,
  userType: string
) => {
  const uploadPromise = new Promise<void>((resolve, reject) => {
    const stream = cloudinary.v2.uploader.upload_stream(
      cloudinaryCardConfig(req.user?._id, userType, req),
      (error: UploadApiErrorResponse | undefined, result: UploadApiResponse | undefined) => {
        if (error) {
          reject(error);
          throw new AppError(error.message, error.http_code);
        } else {
          if (result?.url) {
            if (typeof req.body.content === 'string') {
              req.body.content = JSON.parse(req.body.content);
            }
            req.body.content['audio'] = result?.secure_url;
          }
          resolve();
        }
      }
    );

    stream.write(image);
    stream.end();
  });

  return uploadPromise;
};

export const uploadImgExerciseCloudinary = async (
  image: Buffer,
  req: publicRequest,
  userType: string
): Promise<void> => {
  const uploadPromise = new Promise<void>((resolve, reject) => {
    const stream = cloudinary.v2.uploader.upload_stream(
      cloudinaryUserConfig(req.user?._id, userType, req.query, req),
      (error: UploadApiErrorResponse | undefined, result: UploadApiResponse | undefined) => {
        if (error) {
          reject(error);
          throw new AppError(error.message, error.http_code);
        } else {
          if (result) {
            if (typeof req.body.content === 'string') {
              req.body.content = JSON.parse(req.body.content);
            }

            req.body.content['photo'] = result.secure_url;
          }
          resolve();
        }
      }
    );
    stream.write(image);
    stream.end();
  });

  return uploadPromise;
};

export default cloudinary;
