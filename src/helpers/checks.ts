import 'dotenv/config';

import {
  environment,
  port,
  baseUrl,
  db,
  corsUrl,
  logDirectory,
  cloud_name,
  api_key,
  api_secret,
  cloudinaryUrl
} from '../config/envVar';
import { EMOJIS } from '../constants/emojis';

export const checkAllEnsAreNotEmpty = () => {
  const envs = [
    environment,
    port,
    baseUrl,
    db.name,
    db.host,
    db.port,
    corsUrl,
    logDirectory,
    cloud_name,
    api_key,
    api_secret,
    cloudinaryUrl
  ];
  envs.forEach((env) => {
    if (!env) {
      console.error(`\n${EMOJIS.PROHIBITED}\tOne of the environment variables is not set! \n`);
      process.exit(0);
    }
  });
  console.info(`\n${EMOJIS.SUCCESS}\tAll environment variables are set! \n`);
};

checkAllEnsAreNotEmpty();
