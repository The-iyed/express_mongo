import { db, environment } from "../config/envVar";
import mongoose from 'mongoose';
import Logger from "../core/Logger";
import { EMOJIS } from "../constants/emojis";




const dbURI = `mongodb+srv://${db.name}:${db.password}@cluster0.gd8nlep.mongodb.net/?retryWrites=true&w=majority`;
mongoose.set('strictQuery', false);


export async function connect() {
    await mongoose
        .connect(dbURI)
        .then(()=>{
            Logger.info("Mongoose connection done")
        })
        .catch((e:Error)=>{
            Logger.info("Mongoose connection error")
            Logger.error(e);
        });
}

connect()

mongoose.connection.on("connected",()=>{
    Logger.info("Mongoose default connection open to " + db.host + EMOJIS.CHECK_MARK);
})

mongoose.connection.on('error',(err)=>{
    Logger.error('Mongoose default connection error : ' + err + EMOJIS.NO_ENTRY);
})

mongoose.connection.on('disconnected',()=>{
    Logger.info(
        'Mongoose default connection disconnected' + EMOJIS.RAISED_HAND_WITH_FINGERS_SPLAYED
    )
});

process.on('SIGINT', () => {
    //@ts-ignore
    mongoose.connection.close(() => {
      Logger.info('Mongoose default connection disconnected through app termination');
      process.exit(0);
    });
});