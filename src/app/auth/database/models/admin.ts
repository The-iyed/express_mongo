import mongoose, { Schema, model, ObjectId, PaginateOptions, Document } from 'mongoose';
import paginate from 'mongoose-paginate-v2';
import { Role } from './role';
import { passwordHashing } from '../../../../helpers/auth.utils';
import { softDeletePlugin } from '../../../../plugin/softDelete';
import { SoftDeletePaginateModel } from '../../../../interface/models';

export interface Admin extends Document {
  _id: ObjectId;
  fullName?: string;
  email: string;
  password: string;
  role?: Role[];
  confirmPassword?: string;
  photo?: string;
}

export const schema: Schema = new Schema<Admin>(
  {
    fullName: String,
    email: {
      type: String,
      unique: true,
    },
    password: {
      type: String,
      select: false,
    },
    confirmPassword: String,

    photo: {
      type: String,
      default: 'https://res.cloudinary.com/du87gp2zy/image/upload/v1686726041/default_ya0qoe.jpg',
    },
    role: {
      type: [mongoose.Types.ObjectId],
      ref: 'Role',
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

schema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();
  this.password = await passwordHashing(this.password);
  this.confirmPassword = undefined;
  next();
});

schema.plugin(paginate);
schema.plugin(softDeletePlugin, 'Admin');
export const Admin: SoftDeletePaginateModel<Admin> = model<Admin, SoftDeletePaginateModel<Admin>>(
  'Admin',
  schema
)
