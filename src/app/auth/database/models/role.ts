import { Schema, model , Document , PaginateOptions } from "mongoose";
import paginate from 'mongoose-paginate-v2';
import { SoftDeletePaginateModel } from "../../../../interface/models";




export interface Role extends Document {
    roleName?: string ;
    permission? : string ;
}


export const schema : Schema = new Schema<Role>(
    {
        roleName:{
            type:String,
            unique: [true,'this role is already created']
        },
        permission:{
            type : [String],
            ref: 'Role',
        }
    },
    {
        timestamps:true,
        toJSON : {virtuals : true},
        toObject : { virtuals : true }
    }
)

schema.plugin(paginate)

export const Role : SoftDeletePaginateModel<Role> = model<Role, SoftDeletePaginateModel<Role>>(
    'Role',
    schema
)